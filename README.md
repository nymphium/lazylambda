untyped lambda + integer and let binding in Typed Racket
===

```
> (require "syntax.rkt" "main.rkt")
>
(run (App
  (Lamb "z" (Lamb "_" (Var "z")))
  (App
    (Lamb "y" (App (Var "y") (Var "y")))
    (Lamb "y" (App (Var "y") (Var "y"))))))
- : Value
(Lamb "_" (Var "z"))
```

