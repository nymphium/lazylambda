#lang typed/racket

(require "syntax.rkt")

(provide (all-defined-out))

(: lookup (-> Env String (U Term Value)))
(define (lookup env key)
  (match env
    ['() (error (format "not found: ~a" key))]
    [(cons (cons k v) env~)
     (if (equal? k key)
       v
       (lookup env~ key))]))

(: update (-> Env String Value Env))
(define (update env key val)
  (match env
    ['() (error (format "not found: ~a" key))]
    [(cons (cons k v) env~)
     (if (equal? k key)
       (cons (cons k val) env~)
       (cons (cons k v) (update env~ key val)))]))

(: append (-> Env String (U Term Value) Env))
(define (append env k vt)
  (#{cons @ Elem Any} (#{cons @ String (U Term Value)} k vt) env))

(: arith (-> Env Term Term (-> Exact-Rational Exact-Rational Exact-Rational) (Pairof Value Env)))
(define (arith env t1 t2 fn)
  (match-let* ([(cons (Int v1) e1~) (eval env t1)] [(cons (Int v2) e2~) (eval e1~ t2)])
    (cons (Int (fn v1 v2)) e2~)))

(: eval (-> Env Term (Pairof Value Env)))
(define (eval env t)
  (match t
    [(Int _) (cons t env)]
    [(Add t1 t2)  (arith env t1 t2 +)]
    [(Diff t1 t2) (arith env t1 t2 -)]
    [(Sum t1 t2)  (arith env t1 t2 *)]
    [(Div t1 t2)  (arith env t1 t2 /)]
    [(Let x t~ b) (eval (append env x t~) b)]
    [(Var x)
     (let ([v (lookup env x)])
       (if (Lamb? v)
         (cons v env)
         (match-let ([(cons v~ env~) (eval env v)])
           (cons v~ (update env~ x v~)))))]
    [(App (Lamb x b) arg) (eval (append env x arg) b)]
    [(App fn arg)
     (match-let ([(cons fn env~) (eval env fn)])
       (eval env~ (App fn arg)))]
    [(Lamb _ _) (cons t env)]))

(: run (-> Term Value))
(define (run t)
  (match-let ([(cons v e) (eval '() t)]) v))
